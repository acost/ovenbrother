import { Cuisson, StatutCuisson } from '../../core/models/cuisson.model';
import { CuissonState } from './cuisson.reducer';

export const createCuisson = ({
  id = 'Default ID',
  programme = 'Default Programme Name',
  description = 'Default Description',
  observations = 'Default Observations',
  cones = [],
  debut = 0,
  finCuisson = 0,
  fin = 0,
  statut = StatutCuisson.OK
} = {}): Cuisson => ({
  id,
  programme,
  description,
  observations,
  cones,
  debut,
  finCuisson,
  fin,
  statut
});

export const createCuissonState = (): CuissonState => ({
  cuissons: [createCuisson({ id: '1' }), createCuisson({ id: '2' }), createCuisson({ id: '3' })],
  loading: false
});
