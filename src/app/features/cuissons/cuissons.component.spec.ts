import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { provideMockStore } from '@ngrx/store/testing';
import { SharedModule } from '@shared/shared.module';
import { AppState } from '@store/app.reducer';
import { CuissonsComponent } from './cuissons.component';

describe('CuissonsComponent', () => {
  const initialState: AppState = {
    programmes: {
      loading: false,
      programmes: []
    },
    cuissons: {
      loading: false,
      cuissons: []
    },
    user: {
      loading: false,
    },
  };

  let component: CuissonsComponent;
  let fixture: ComponentFixture<CuissonsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CuissonsComponent],
      imports: [SharedModule, FormsModule],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [provideMockStore({ initialState })]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuissonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
