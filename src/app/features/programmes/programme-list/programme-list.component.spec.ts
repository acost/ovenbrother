import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { NO_ERRORS_SCHEMA } from "@angular/core";
import { SharedModule } from "@shared/shared.module";
import { provideMockStore } from "@ngrx/store/testing";
import { ProgrammeState } from "@store/reducers/programme.reducer";
import { ProgrammeItemComponent } from "../programme-item/programme-item.component";
import { ProgrammeListComponent } from "./programme-list.component";

describe("ProgrammeListComponent", () => {
  const initialState: ProgrammeState = {
    loading: false,
    programmes: [],
  };

  let component: ProgrammeListComponent;
  let fixture: ComponentFixture<ProgrammeListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProgrammeListComponent, ProgrammeItemComponent],
      imports: [SharedModule],
      providers: [provideMockStore({ initialState })],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
