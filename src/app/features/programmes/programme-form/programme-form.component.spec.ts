import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { FormsModule } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { PalierGraphComponent } from "@shared/palier-graph/palier-graph.component";
import { PalierListComponent } from "../palier-list/palier-list.component";
import { ProgrammeCardComponent } from "../programme-card/programme-card.component";
import { PalierInputListComponent } from "./palier-input-list/palier-input-list.component";
import { PalierInputComponent } from "./palier-input-list/palier-input/palier-input.component";
import { ProgrammeFormComponent } from "./programme-form.component";

describe("ProgrammeFormComponent", () => {
  let component: ProgrammeFormComponent;
  let fixture: ComponentFixture<ProgrammeFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProgrammeFormComponent,
        ProgrammeCardComponent,
        PalierListComponent,
        PalierGraphComponent,
        PalierInputComponent,
        PalierInputListComponent,
      ],
      imports: [SharedModule, FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
