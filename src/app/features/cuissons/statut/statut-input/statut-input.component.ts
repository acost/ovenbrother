import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { StatutCuisson } from '@core/models/cuisson.model';

@Component({
  selector: 'app-statut-input',
  templateUrl: './statut-input.component.html',
  styles: [],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => StatutInputComponent),
      multi: true
    }
  ]
})
export class StatutInputComponent implements ControlValueAccessor {
  @Input() key: string;
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('statut') currentStatut: StatutCuisson;
  @Input() readOnly = false;

  StatutCuisson = StatutCuisson;

  onChange: (_: any) => void;
  onTouched: () => void;

  get statut() {
    return this.currentStatut;
  }

  set statut(selection) {
    if (selection) {
      this.currentStatut = selection;
      this.onChange && this.onChange(selection);
      this.onTouched && this.onTouched();
    }
  }


  buildButtonClass(statut: StatutCuisson, activeStyleClass: string) {
    const isSelected = this.currentStatut === statut;
    if (isSelected) {
      return 'active btn ' + activeStyleClass;
    } else if (this.readOnly) {
      return 'disabled btn btn-outline-secondary';
    } else {
      return 'btn btn-outline-secondary';
    }
  }

  writeValue(value: StatutCuisson) {
    if (value !== undefined) {
      this.statut = value;
      this.onChange(this.statut);
    }
  }

  registerOnChange(fn) { this.onChange = fn; }
  registerOnTouched(fn) { this.onTouched = fn; }

  doSelect(value: StatutCuisson) {
    this.statut = value;
    this.onChange(this.statut);
  }

}
