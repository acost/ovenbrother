import { createAction, props, union } from '@ngrx/store';
import { Cuisson } from '@core/models/cuisson.model';

export const loadCuissons = createAction('[Cuisson] LOAD');

export const loadCuissonsSuccess = createAction('[Cuisson] LOAD Success', props<{ cuissons: Cuisson[] }>());

export const loadCuissonsError = createAction('[Cuisson] LOAD Error', props<{ error?: string }>());

export const addCuisson = createAction('[Cuisson] CREATE', props<{ cuisson: Cuisson }>());

export const updateCuisson = createAction('[Cuisson] UPDATE', props<{ cuisson: Cuisson }>());

export const deleteCuisson = createAction('[Cuisson] DELETE', props<{ cuisson: Cuisson }>());

const actions = union({ loadCuissons, loadCuissonsSuccess, loadCuissonsError, addCuisson, updateCuisson, deleteCuisson });
export type CuissonActionsUnion = typeof actions;
