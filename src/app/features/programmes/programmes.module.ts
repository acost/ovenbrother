import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { SharedModule } from "@shared/shared.module";
import { PalierListComponent } from "./palier-list/palier-list.component";
import { ProgrammeCardComponent } from "./programme-card/programme-card.component";
import { PalierInputListComponent } from "./programme-form/palier-input-list/palier-input-list.component";
import { PalierInputComponent } from "./programme-form/palier-input-list/palier-input/palier-input.component";
import { ProgrammeFormComponent } from "./programme-form/programme-form.component";
import { ProgrammeItemComponent } from "./programme-item/programme-item.component";
import { ProgrammeListComponent } from "./programme-list/programme-list.component";
import { ProgrammesRoutingModule } from "./programmes-routing.module";
import { ProgrammesComponent } from "./programmes.component";

export const COMPONENTS = [
  ProgrammesComponent,
  ProgrammeItemComponent,
  ProgrammeFormComponent,
  ProgrammeListComponent,
  ProgrammeCardComponent,
  PalierInputComponent,
  PalierInputListComponent,
  PalierListComponent,
];

@NgModule({
  imports: [
    ProgrammesRoutingModule,
    SharedModule,
    FormsModule,
    TooltipModule.forRoot(),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class ProgrammesModule {}
