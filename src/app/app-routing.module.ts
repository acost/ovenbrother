import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './core/not-found/not-found.component';
import { AngularFireAuthGuard } from '@angular/fire/compat/auth-guard';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'programmes',
    loadChildren: () => import('./features/programmes/programmes.module').then(m => m.ProgrammesModule),
    canActivate: [AngularFireAuthGuard]
  },
  {
    path: 'cuissons',
    loadChildren: () => import('./features/cuissons/cuissons.module').then(m => m.CuissonsModule),
    canActivate: [AngularFireAuthGuard]
  },
  { path: '**', component: NotFoundComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
