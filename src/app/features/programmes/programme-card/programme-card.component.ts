import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import { Programme } from "@core/models/programme.model";

@Component({
  selector: "app-programme-card",
  templateUrl: "./programme-card.component.html",
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgrammeCardComponent {
  @Input() programme: Programme;

  @Output() edit = new EventEmitter();

  @Output() delete = new EventEmitter();

  triggerEdition() {
    this.edit.emit();
  }

  triggerDelete() {
    this.delete.emit();
  }
}
