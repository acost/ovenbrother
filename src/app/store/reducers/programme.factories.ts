import { Programme } from '../../core/models/programme.model';
import { ProgrammeState } from './programme.reducer';

export const createProgramme = ({
  id = 'Default ID',
  numero = 1,
  nom = 'Default Programme Name',
  description = 'Default Description',
  paliers = [],
  editionDate = 0
} = {}): Programme => ({
  id,
  numero,
  nom,
  description,
  paliers,
  editionDate
});

export const createProgrammeState = (): ProgrammeState => ({
  programmes: [createProgramme({ id: '1' }), createProgramme({ id: '2' }), createProgramme({ id: '3' })],
  loading: false
});
