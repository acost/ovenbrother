[![Storybook](https://cdn.jsdelivr.net/gh/storybookjs/brand@master/badge/badge-storybook.svg)](https://ovenbrother-storybook.web.app/)

<div align="center">

![alt text](src/assets/img/logo.png "Oven Brother")
# Oven Brother
The one who takes care of it.
</div>

## Getting started

Run `npm install` in order to gather every required dependency.

## Running

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`.\
Run `npm run storybook` for a storybook server. Navigate to `http://localhost:6006`.

## Building

Run `npm run build:prod` to build oven-brother under `dist/ng-oven-brother`.\
Run `npm run storybook:build` to build storybook under `dist/storybook`.

## Deploying

Run `firebase deploy -m "[facultative message]"` to deploy everything under the project.\
Run `firebase deploy --only hosting -m "[facultative message]"` to deploy every hosting related target.

Run `firebase deploy --only hosting:app -m "[facultative message]"` to deploy only oven-brother.\
Run `firebase deploy --only hosting:storybook -m "[facultative message]"` to deploy only the storybook.
