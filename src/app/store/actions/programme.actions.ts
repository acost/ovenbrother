import { createAction, props, union } from '@ngrx/store';
import { Programme } from '@core/models/programme.model';

export const loadProgrammes = createAction('[Programme] LOAD');

export const loadProgrammesSuccess = createAction('[Programme] LOAD Success', props<{ programmes: Programme[] }>());

export const loadProgrammesError = createAction('[Programme] LOAD Error', props<{ error?: string }>());

export const addProgramme = createAction('[Programme] CREATE', props<{ programme: Programme }>());

export const updateProgramme = createAction('[Programme] UPDATE', props<{ programme: Programme }>());

export const deleteProgramme = createAction('[Programme] DELETE', props<{ programme: Programme }>());

const actions = union({ loadProgrammes, loadProgrammesSuccess, loadProgrammesError, addProgramme, updateProgramme, deleteProgramme });
export type ProgrammeActionsUnion = typeof actions;
