import { User } from '@core/models/user.model';
import { UserState } from './user.reducer';

export const createUser = ({ uid = 'Default UID', displayName = 'Default Display Name' } = {}): User => ({
  uid,
  displayName
});

export const createUserState = (): UserState => ({
  user: createUser(),
  loading: false,
  error: ''
});
