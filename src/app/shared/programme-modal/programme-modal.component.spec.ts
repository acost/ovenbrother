import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { PalierGraphComponent } from "../palier-graph/palier-graph.component";
import { SharedModule } from "../shared.module";
import { ProgrammeModalComponent } from "./programme-modal.component";

describe("ProgrammeModalComponent", () => {
  let component: ProgrammeModalComponent;
  let fixture: ComponentFixture<ProgrammeModalComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProgrammeModalComponent, PalierGraphComponent],
      imports: [SharedModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
