import { Component, forwardRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Palier } from '@core/models/palier.model';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-palier-input-list',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PalierInputListComponent),
      multi: true
    }
  ],
  templateUrl: './palier-input-list.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PalierInputListComponent implements ControlValueAccessor {

  val: Palier[] = [];

  onChange: (_: any) => void;
  onTouched: () => void;

  constructor(private cdr: ChangeDetectorRef) { }

  get paliers() {
    return this.val;
  }

  set paliers(val) {
    this.val = val;
    if (this.val && this.val.length > 0) {
      // Permet de supprimer des paliers correctement, même sans id initialisé.
      this.val.forEach((palier, index) => palier.id = palier.id ? palier.id : `palier-${index}-${Date.now()}`);
    }
    this.onChange && this.onChange(val);
    this.onTouched && this.onTouched();
    this.cdr.detectChanges();
  }

  addPalier() {
    this.paliers = [...this.paliers, this.initPalier()];
  }

  private initPalier(): Palier {
    return { id: `palier-${Date.now()}` } as Palier;
  }

  removePalier(id: string) {
    this.paliers = this.paliers.filter(palier => palier.id !== id);
  }

  writeValue(value: Palier[]) {
    // On ne permet pas d'avoir 0 paliers : Si cela arrive, on force la présence d'un nouveau.
    this.paliers = value && value.length > 0 ? JSON.parse(JSON.stringify(value)) : [this.initPalier()];
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }
}
