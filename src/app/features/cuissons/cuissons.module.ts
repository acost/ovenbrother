import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { CuissonCardComponent } from "./cuisson-card/cuisson-card.component";
import { CuissonTableComponent } from "./cuisson-table/cuisson-table.component";
import { CuissonsRoutingModule } from "./cuissons-routing.module";
import { CuissonsComponent } from "./cuissons.component";
import { StartCuissonFormComponent } from "./start-cuisson-form/start-cuisson-form.component";
import { StatutIconComponent } from "./statut/statut-icon/statut-icon.component";
import { StatutInputComponent } from "./statut/statut-input/statut-input.component";
import { StopCuissonFormComponent } from "./stop-cuisson-form/stop-cuisson-form.component";

export const COMPONENTS = [
  CuissonsComponent,
  CuissonTableComponent,
  StatutIconComponent,
  StatutInputComponent,
  StartCuissonFormComponent,
  StopCuissonFormComponent,
  CuissonCardComponent,
];

@NgModule({
  imports: [CuissonsRoutingModule, SharedModule, FormsModule],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class CuissonsModule {}
