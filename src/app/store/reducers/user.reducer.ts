import { createReducer, on } from '@ngrx/store';
import * as UserActions from '../actions/user.actions';
import { User } from '@core/models/user.model';

export interface UserState {
  user?: User;
  loading: boolean;
  error?: string;
}

export const defaultUser: User = { uid: null, displayName: 'GUEST' };

export const initialState: UserState = {
  loading: false
};

const programmeReducer = createReducer(
  initialState,
  on(UserActions.getUser, state => ({
    ...state,
    loading: true
  })),
  on(UserActions.googleLogin, state => ({
    ...state,
    loading: true
  })),
  on(UserActions.logout, state => ({
    ...state,
    loading: true
  })),
  on(UserActions.authenticated, (state, { uid, displayName }) => ({
    ...state,
    loading: false,
    user: { uid, displayName } as User
  })),
  on(UserActions.notAuthenticated, state => ({
    ...state,
    loading: false,
    user: defaultUser
  })),
  on(UserActions.authError, (state, { error }) => ({
    ...state,
    error,
    loading: false
  }))
);

export function reducer(state: UserState = initialState, action: UserActions.UserActionsUnion) {
  return programmeReducer(state, action);
}
