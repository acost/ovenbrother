import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CuissonsComponent } from './cuissons.component';

export const routes: Routes = [
  { path: '', component: CuissonsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuissonsRoutingModule { }
