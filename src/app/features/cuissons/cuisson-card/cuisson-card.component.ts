import { Component, EventEmitter, Input, Output } from '@angular/core';
import { differenceInHours, differenceInMinutes } from 'date-fns';
import { Cuisson } from '@core/models/cuisson.model';
import { Programme } from '@core/models/programme.model';

@Component({
  selector: 'app-cuisson-card',
  templateUrl: './cuisson-card.component.html',
  styleUrls: ['./cuisson-card.component.scss']
})
export class CuissonCardComponent {

  @Input() cuisson: Cuisson;
  @Input() programme: Programme;

  @Output() delete = new EventEmitter<Cuisson>();

  isCuissonActive = cuisson => !cuisson.fin;

  calculateDureeCuisson(cuisson: Cuisson): string {
    const dateFin = this.isCuissonActive(cuisson) ? Date.now() : cuisson.fin;
    const hours = differenceInHours(dateFin, cuisson.debut).toString().padStart(2, '0');
    const minutes = (differenceInMinutes(dateFin, cuisson.debut) % 60).toString().padStart(2, '0');
    return `${hours}h${minutes}`;
  }

  triggerDelete(cuisson: Cuisson) {
    this.delete.emit(cuisson);
  }
}
