import { createAction, props, union } from '@ngrx/store';

export const notifySuccess = createAction('[Notification] Success', props<{ message?: string }>());
export const notifyError = createAction('[Notification] Error', props<{ message?: string }>());

const actions = union({ notifySuccess, notifyError });
export type NotificationActionsUnion = typeof actions;
