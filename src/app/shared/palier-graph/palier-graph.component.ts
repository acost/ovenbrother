import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy,
} from "@angular/core";
import { Palier } from "@core/models/palier.model";
import { ChartConfiguration, ChartType } from "chart.js";
import {
  parse,
  differenceInMinutes,
  differenceInHours,
  differenceInMilliseconds,
} from "date-fns";

@Component({
  selector: "app-palier-graph",
  templateUrl: "./palier-graph.component.html",
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PalierGraphComponent implements OnInit, OnChanges {
  @Input() paliers: Palier[];

  public lineChartData: ChartConfiguration["data"] = {
    datasets: [
      {
        data: [],
        label: "Température atteinte",
        pointBorderColor: "rgba(75,192,192,1)",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 3,
        pointRadius: 2,
        pointHitRadius: 20,
        backgroundColor: "rgba(75,192,192,0.4)",
        borderColor: "rgba(75,192,192,1)",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        borderWidth: 3,
      },
    ],
    labels: [],
  };
  public lineChartOptions: ChartConfiguration["options"] = {
    animation: false,
    responsive: true,
    scales: {
      yAxes: {
        ticks: {
          autoSkip: true,
          maxTicksLimit: 4,
        },
      },
    },
    elements: {
      line: {
        fill: false,
        tension: 0.1,
      },
    },
  };
  public lineChartLegend = false;
  public lineChartType: ChartType = "line";
  public lineChartPlugins = [];

  ngOnInit() {
    this.lineChartData.labels = [...this.paliersToLabels(this.paliers)];
    this.lineChartData.datasets[0].data = [
      ...this.paliersToTemperatures(this.paliers),
    ];
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.lineChartData.labels = [...this.paliersToLabels(this.paliers)];
    this.lineChartData.datasets[0].data = [
      ...this.paliersToTemperatures(this.paliers),
    ];
  }

  paliersToLabels = (paliers: Palier[]): string[] => {
    if (!paliers) {
      return [];
    }
    let totalDuration = 0;
    const labels: string[] = Object.keys(paliers).map((index) => {
      if (paliers[index].duree) {
        totalDuration += this.getMillisecondsForDurationString(
          paliers[index].duree
        );
      }
      return this.convertMillisecondsForDisplay(totalDuration);
    });
    return ["00:00", ...labels];
  };

  private getMillisecondsForDurationString(durationString: string): number {
    const baseHour = parse("00:00", "HH:mm", 0);
    return differenceInMilliseconds(
      parse(durationString, "HH:mm", 0),
      baseHour
    );
  }

  private convertMillisecondsForDisplay(milliseconds: number): string {
    const hours = differenceInHours(milliseconds, 0)
      .toString()
      .padStart(2, "0");
    const formattedMinutes = (differenceInMinutes(milliseconds, 0) % 60)
      .toString()
      .padStart(2, "0");
    return `${hours}:${formattedMinutes}`;
  }

  paliersToTemperatures = (paliers: Palier[]): number[] => {
    if (!paliers) {
      return [];
    }
    const temperatures = Object.keys(paliers).map(
      (index) => paliers[index].temperature
    );
    return [0, ...temperatures];
  };
}
