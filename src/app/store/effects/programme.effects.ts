import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError, withLatestFrom } from 'rxjs/operators';
import { ProgrammeService } from '@core/services/programme.service';
import * as ProgrammeActions from './../actions/programme.actions';
import { Store, select } from '@ngrx/store';
import { UserState } from '../reducers/user.reducer';
import { selectUser } from '../selectors/user.selector';
import { Programme } from '@core/models/programme.model';
import { notifyError, notifySuccess } from '../actions/notification.actions';

@Injectable()
export class ProgrammeEffects {

    loadProgrammes$ = createEffect(() => this.actions$.pipe(
        ofType(ProgrammeActions.loadProgrammes),
        withLatestFrom(this.store.pipe(select(selectUser))),
        mergeMap(([, user]) => this.programmeService.getProgrammes(user.uid)
            .pipe(
                map((programmes: Programme[]) => ProgrammeActions.loadProgrammesSuccess({ programmes })),
                catchError(err => of(ProgrammeActions.loadProgrammesError({ error: err.message })))
            ))
    )
    );

    addProgrammes$ = createEffect(() => this.actions$.pipe(
        ofType(ProgrammeActions.addProgramme),
        withLatestFrom(this.store.pipe(select(selectUser))),
        mergeMap(([action, user]) => this.programmeService.addProgramme(user.uid, action.programme)
            .pipe(
                map(() => notifySuccess({ message: 'Programme ajouté avec succès.' })),
                catchError(err => of(notifyError({ message: err.message })))
            ))
    )
    );

    updateProgrammes$ = createEffect(() => this.actions$.pipe(
        ofType(ProgrammeActions.updateProgramme),
        withLatestFrom(this.store.pipe(select(selectUser))),
        mergeMap(([action, user]) => this.programmeService.updateProgramme(user.uid, action.programme.id, action.programme)
            .pipe(
                map(() => notifySuccess({ message: 'Programme mis à jour avec succès.' })),
                catchError(err => of(notifyError({ message: err.message })))
            ))
    )
    );

    removeProgrammes$ = createEffect(() => this.actions$.pipe(
        ofType(ProgrammeActions.deleteProgramme),
        withLatestFrom(this.store.pipe(select(selectUser))),
        mergeMap(([action, user]) => this.programmeService.removeProgramme(user.uid, action.programme.id)
            .pipe(
                map(() => notifySuccess({ message: 'Programme supprimé avec succès.' })),
                catchError(err => of(notifyError({ message: err.message })))
            ))
    )
    );

    constructor(
        private actions$: Actions<ProgrammeActions.ProgrammeActionsUnion>,
        private programmeService: ProgrammeService,
        private store: Store<UserState>
    ) { }
}
