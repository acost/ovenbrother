import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { FormsModule } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { PalierGraphComponent } from "@shared/palier-graph/palier-graph.component";
import { PalierListComponent } from "../palier-list/palier-list.component";
import { ProgrammeCardComponent } from "../programme-card/programme-card.component";
import { PalierInputListComponent } from "../programme-form/palier-input-list/palier-input-list.component";
import { PalierInputComponent } from "../programme-form/palier-input-list/palier-input/palier-input.component";
import { ProgrammeFormComponent } from "../programme-form/programme-form.component";
import { ProgrammeItemComponent } from "./programme-item.component";

describe("ProgrammeItemComponent", () => {
  let component: ProgrammeItemComponent;
  let fixture: ComponentFixture<ProgrammeItemComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProgrammeItemComponent,
        ProgrammeFormComponent,
        ProgrammeCardComponent,
        PalierListComponent,
        PalierGraphComponent,
        PalierInputComponent,
        PalierInputListComponent,
      ],
      imports: [SharedModule, FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammeItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
