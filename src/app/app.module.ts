import { ErrorHandler, Injectable, NgModule } from "@angular/core";
import { AngularFireAuthGuard } from "@angular/fire/compat/auth-guard";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CoreModule } from "./core/core.module";
import { HomeComponent } from "./home/home.component";

import * as Sentry from "@sentry/browser";

Sentry.init({
  dsn: "https://0dbeeb2afeb546edbdf86121bae35c75@sentry.io/1825394",
});

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  handleError(error) {
    const eventId = Sentry.captureException(error.originalError || error);
    Sentry.showReportDialog({ eventId, lang: "fr" });
  }
}

@NgModule({
  imports: [CoreModule, AppRoutingModule],
  declarations: [AppComponent, HomeComponent],
  providers: [
    AngularFireAuthGuard,
    { provide: ErrorHandler, useClass: SentryErrorHandler },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
