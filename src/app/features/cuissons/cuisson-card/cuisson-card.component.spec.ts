import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '@shared/shared.module';
import { getTime, parse } from 'date-fns';
import { Cuisson } from '@core/models/cuisson.model';
import { CuissonCardComponent } from '../cuisson-card/cuisson-card.component';
import { StatutIconComponent } from '../statut/statut-icon/statut-icon.component';

describe('CuissonCardComponent', () => {
  let component: CuissonCardComponent;
  let fixture: ComponentFixture<CuissonCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [CuissonCardComponent, StatutIconComponent],
      imports: [SharedModule, NoopAnimationsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuissonCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('calculateDureeCuisson', () => {
    it('should return correct value', () => {
      const cuissonWithDates: Cuisson = {
        debut: getTime(parse('2010-10-20 4:30', 'yyyy-MM-dd H:mm', new Date())),
        fin: getTime(parse('2010-10-20 7:00', 'yyyy-MM-dd H:mm', new Date()))
      } as Cuisson;
      // when
      const formattedDuration = component.calculateDureeCuisson(cuissonWithDates);
      // then
      expect(formattedDuration).toBe('02h30');
    });

    it('should pad to 2 digits', () => {
      const cuissonWithDates: Cuisson = {
        debut: getTime(parse('2010-10-20 4:30', 'yyyy-MM-dd H:mm', new Date())),
        fin: getTime(parse('2010-10-20 4:32', 'yyyy-MM-dd H:mm', new Date()))
      } as Cuisson;
      // when
      const formattedDuration = component.calculateDureeCuisson(cuissonWithDates);
      // then
      expect(formattedDuration).toBe('00h02');
    });

    it('should display big values', () => {
      const cuissonWithDates: Cuisson = {
        debut: getTime(parse('2010-10-10 4:30', 'yyyy-MM-dd H:mm', new Date())),
        fin: getTime(parse('2010-10-20 4:54', 'yyyy-MM-dd H:mm', new Date()))
      } as Cuisson;
      // when
      const formattedDuration = component.calculateDureeCuisson(cuissonWithDates);
      // then
      expect(formattedDuration).toBe('240h24');
    });
  });
});
