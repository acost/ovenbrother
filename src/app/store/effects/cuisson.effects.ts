import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError, withLatestFrom } from 'rxjs/operators';
import * as CuissonActions from '../actions/cuisson.actions';
import { CuissonService } from '@core/services/cuisson.service';
import { select, Store } from '@ngrx/store';
import { selectUser } from '../selectors/user.selector';
import { UserState } from '../reducers/user.reducer';
import { notifySuccess, notifyError } from '../actions/notification.actions';

@Injectable()
export class CuissonEffects {
  loadCuissons$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CuissonActions.loadCuissons),
      withLatestFrom(this.store.pipe(select(selectUser))),
      mergeMap(([, user]) =>
        this.cuissonService.getCuissons(user.uid).pipe(
          map(cuissons => CuissonActions.loadCuissonsSuccess({ cuissons })),
          catchError(err => of(CuissonActions.loadCuissonsError({ error: err.message })))
        )
      )
    )
  );

  addCuisson$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CuissonActions.addCuisson),
      withLatestFrom(this.store.pipe(select(selectUser))),
      mergeMap(([action, user]) =>
        this.cuissonService.addCuisson(user.uid, action.cuisson).pipe(
          map(_ => notifySuccess({ message: 'Programme ajouté avec succès.' })),
          catchError(err => of(notifyError({ message: err.message })))
        )
      )
    )
  );

  updateCuisson$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CuissonActions.updateCuisson),
      withLatestFrom(this.store.pipe(select(selectUser))),
      mergeMap(([action, user]) =>
        this.cuissonService.updateCuisson(user.uid, action.cuisson.id, action.cuisson).pipe(
          map(_ => notifySuccess({ message: 'Programme mis à jour avec succès.' })),
          catchError(err => of(notifyError({ message: err.message })))
        )
      )
    )
  );

  removeCuisson$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CuissonActions.deleteCuisson),
      withLatestFrom(this.store.pipe(select(selectUser))),
      mergeMap(([action, user]) =>
        this.cuissonService.removeCuisson(user.uid, action.cuisson.id).pipe(
          map(_ => notifySuccess({ message: 'Programme supprimé avec succès.' })),
          catchError(err => of(notifyError({ message: err.message })))
        )
      )
    )
  );

  constructor(
    private actions$: Actions<CuissonActions.CuissonActionsUnion>,
    private cuissonService: CuissonService,
    private store: Store<UserState>
  ) {}
}
