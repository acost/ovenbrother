import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatDividerModule } from "@angular/material/divider";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSortModule } from "@angular/material/sort";
import { MatTableModule } from "@angular/material/table";
import { RouterModule } from "@angular/router";
import { NgChartsModule } from "ng2-charts";
import { ModalModule } from "ngx-bootstrap/modal";
import { PalierGraphComponent } from "./palier-graph/palier-graph.component";
import { ProgrammeModalComponent } from "./programme-modal/programme-modal.component";

export const MATERIAL_COMPONENTS = [
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatIconModule,
  MatDividerModule,
  MatButtonModule,
  MatListModule,
];

@NgModule({
  imports: [
    // vendor
    CommonModule,
    RouterModule,
    NgChartsModule,
    ModalModule.forRoot(),
    ...MATERIAL_COMPONENTS,

    // local
    CommonModule,
  ],
  declarations: [ProgrammeModalComponent, PalierGraphComponent],
  exports: [
    // vendor
    CommonModule,
    RouterModule,
    ...MATERIAL_COMPONENTS,

    // local
    ProgrammeModalComponent,
    PalierGraphComponent,
  ],
})
export class SharedModule {}
