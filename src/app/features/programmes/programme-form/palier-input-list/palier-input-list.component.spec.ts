import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { FormsModule } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { PalierInputListComponent } from "./palier-input-list.component";
import { PalierInputComponent } from "./palier-input/palier-input.component";

describe("PalierInputListComponent", () => {
  let component: PalierInputListComponent;
  let fixture: ComponentFixture<PalierInputListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PalierInputListComponent, PalierInputComponent],
      imports: [SharedModule, FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PalierInputListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
