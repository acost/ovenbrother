import { Injectable } from '@angular/core';
import { Observable, from, throwError } from 'rxjs';
import { Programme } from '../models/programme.model';
import { map, flatMap } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class ProgrammeService {

  constructor(private db: AngularFirestore) { }

  getProgrammes(uid: string): Observable<{ [id: number]: Programme }> {
    if (!uid) {
      return from([]);
    }
    const collection = this.db.collection<Programme>(`users/${uid}/programmes`);
    return collection.snapshotChanges().pipe(map(values =>
      values.map(c => ({ ...c.payload.doc.data(), id: c.payload.doc.id } as Programme)))) as Observable<any> as Observable<Programme[]>;
  }

  addProgramme(uid: string, programme: Programme) {
    const collection = this.db.collection<Programme>(`users/${uid}/programmes`);
    return from(collection.add(JSON.parse(JSON.stringify(programme))));
  }

  updateProgramme(uid: string, itemId: string, programme: Programme) {
    const collection = this.db.collection<Programme>(`users/${uid}/programmes`);
    return from(collection.doc(itemId).update(JSON.parse(JSON.stringify(programme))));
  }

  removeProgramme(uid: string, itemId: string) {
    return this.countProgrammeUsages(uid, itemId)
      .pipe(
        flatMap(value => {
          if (value > 0) {
            return throwError({ message: `Suppression impossible : Programme utilisé dans ${value} cuissons.` });
          }
          const collection = this.db.collection<Programme>(`users/${uid}/programmes`);
          return collection.doc(itemId).delete();
        }
        ));
  }

  countProgrammeUsages(uid: string, itemId: string): Observable<number> {
    const linkedCuissons = this.db.collection<Programme>(`users/${uid}/cuissons`, ref => ref.where('programme', '==', itemId));
    return from(linkedCuissons.valueChanges().pipe(map(results => results.length)));
  }
}
