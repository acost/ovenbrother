### [1.1.2](https://gitlab.com/acost/ovenbrother/compare/v1.1.1...v1.1.2) (2019-11-24)


### Corrections de bug

* Correction d'un bug empêchant d'accéder au site depuis iOS ([0fce788](https://gitlab.com/acost/ovenbrother/commit/0fce788dc6d5afd5d2335b6fcaacd85eac91ea36))


### Intégration Continue

* Configuration en tant que Review Apps du storybook fourni en merge request ([772299c](https://gitlab.com/acost/ovenbrother/commit/772299ce0c2a2f41fe430d12ea0494fcdecf1a81))
* Exposition de Storybook & Coverage Report intégrée dans les Merge Requests ([03a86f1](https://gitlab.com/acost/ovenbrother/commit/03a86f13d60e85a1187296ec46f96a49007f3662))
* Optimisation du temps de la CI en parallélisant tests & build ([9ed0650](https://gitlab.com/acost/ovenbrother/commit/9ed0650bdbe58d895366937e5cc454396e44bb61))


### Documentation

* Rajout de l'historique du projet dans le CHANGELOG ([734d1a4](https://gitlab.com/acost/ovenbrother/commit/734d1a408ae112a71f15b5a10bcdaa6983927f5c))

### [1.1.1](https://gitlab.com/acost/ovenbrother/compare/v1.1.0...v1.1.1) (2019-11-19)


### Corrections de bug

* **storybook:** Correction d'erreurs de configuration & passage en version 5.3.0-beta.2 ([cd32dea](https://gitlab.com/acost/ovenbrother/commit/cd32dea99a287355c4a67296d1a0562afba46fa8))

## [1.1.0](https://gitlab.com/acost/ovenbrother/compare/v1.0.0...v1.1.0) (2019-11-18)


### Intégration Continue

* Exposition des rapports TSLint + Coverage dans les merge requests ([b1ad132](https://gitlab.com/acost/ovenbrother/commit/b1ad132d37a28b7f20a1fcb0cec1bd39c773fc32))
* Passage à Jest en tant qu'outil de tests automatisés (retrait de jasmine / karma) ([1b9362e](https://gitlab.com/acost/ovenbrother/commit/1b9362eef33bcb98eac60d22ea2b59a388394d55))
* Tracking des déploiements avec les environnements GitLab ([4afead2](https://gitlab.com/acost/ovenbrother/commit/4afead24106bbf71dc8915e07c83b8e3cd8224ce))


### Fonctionnalités

* Intégration de Sentry pour monitorer les erreurs ([9e37109](https://gitlab.com/acost/ovenbrother/commit/9e37109bb671e30a479ac38553976dc02d83c30d))

## 1.0.0 (2019-11-03)


### Intégration Continue

* Amélioration du format de release note généré ([47c69b4](https://gitlab.com/acost/ovenbrother/commit/47c69b438a15496ff9390fd80869bc746aae5618))
* Amélioration du suivi des déploiements ([033c870](https://gitlab.com/acost/ovenbrother/commit/033c8706b8b90e5e79a883152c443a981a80952b))
* Amélioration du suivi des déploiements ([6b92c1e](https://gitlab.com/acost/ovenbrother/commit/6b92c1e51c7574ffd90843e33e2e1a2caf2e957b))
* Configuration pour release & montée de version ([d821777](https://gitlab.com/acost/ovenbrother/commit/d82177785ec7db99ecebd53a9f126d3fc2d3301a))
* Paliatif à un bug de GitLab entre rules & pipelines de merge results ([af498e9](https://gitlab.com/acost/ovenbrother/commit/af498e94fce94ed889e18a38f2dfa23075f3d027))


### Documentation

* Ajout d'un lien Storybook depuis le projet ([409eb61](https://gitlab.com/acost/ovenbrother/commit/409eb61e4dae4ca7dd0da50b1b57f2953fe4fedf))


### Corrections de bug

* **cuisson:** Correction de l'affichage des dates ([f447bb0](https://gitlab.com/acost/ovenbrother/commit/f447bb0540d01a31cfb1a4f33cfee28f477fb557))

Faits notables avant la 1.0.0
=======

* Développements Angular
    * 2019-11-03: Automatisation des releases avec semantic-release
    * 2019-10-27: Mise en place d'une CI complète
    * 2019-10-24: _Migration de BitBucket vers GitLab_
    * 2019-10-01: Mise en place de Storybook
    * 2019-09-19: Utilisation offline de la PWA + invite de mise à jour
    * 2019-09-19: Migration de Real Time Database vers FireStore
    * 2019-09-12: Refonte du design
    * 2019-09-10: Passage en Progressive Web App
    * 2019-08-25: Création de la galerie de photos
    * 2019-08-23: Upload de fichiers avec Firestorage
    * 2019-08-18: Optimisation du chargement via lazy-loading
    * 2019-08-18: _Décommissionnement de l'app React_
    * 2019-08-10: Intégration de NGRX
    * 2019-04-10: _Démarrage de la migration en Angular_

* Développements React
    * 2018-12-27: _Derniers développements en React_
    * 2018-11-17: Synchronisation avec react-redux-firebase
    * 2018-11-11: Intégration de Redux
    * 2018-11-05: Intégration de l'authentification Firebase
    * 2018-10-29: Premier déploiement sur Firebase
    * 2018-10-27: _Démarrage du projet en React_
