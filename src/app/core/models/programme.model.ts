import { Palier } from './palier.model';

export class Programme {
    id: string;
    nom: string;
    description: string;
    numero: number;
    editionDate: number;
    paliers: Array<Palier>;

    constructor(id?: string, nom?: string, description?: string, numero?: number, editionDate?: number, paliers?: Array<Palier>) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.numero = numero;
        this.editionDate = editionDate;
        this.paliers = paliers;
    }
}
