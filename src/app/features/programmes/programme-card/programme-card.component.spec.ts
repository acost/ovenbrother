import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { SharedModule } from "@shared/shared.module";
import { PalierGraphComponent } from "@shared/palier-graph/palier-graph.component";
import { PalierListComponent } from "../palier-list/palier-list.component";
import { ProgrammeCardComponent } from "./programme-card.component";

describe("ProgrammeCardComponent", () => {
  let component: ProgrammeCardComponent;
  let fixture: ComponentFixture<ProgrammeCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProgrammeCardComponent,
        PalierListComponent,
        PalierGraphComponent,
      ],
      imports: [SharedModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
