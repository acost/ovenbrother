import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { format, getTime, parse } from 'date-fns';
import { Cone, Cuisson } from '@core/models/cuisson.model';
import { Programme } from '@core/models/programme.model';

@Component({
  selector: 'app-stop-cuisson-form',
  templateUrl: './stop-cuisson-form.component.html',
  styles: []
})
export class StopCuissonFormComponent {
  @Input() cuisson: Cuisson = {} as Cuisson;
  @Input() programme: Programme;

  @Output() valider = new EventEmitter<Cuisson>();

  @Input() dateFin: string = format(new Date(), 'yyyy-MM-dd');
  @Input() heureFin: string = format(new Date(), 'HH:mm');

  get programmeText() {
    return this.programme ? this.programme.nom + '(#' + this.programme.numero + ')' : '';
  }

  doSubmit(form: NgForm, event: Event) {
    event.preventDefault();
    const { dateFin, heureFin, observations, statut, ...coneStatuts } = form.value;

    const fin: Date = parse(`${dateFin}, ${heureFin}`, 'yyyy-MM-dd, HH:mm', new Date());
    const cones: Cone[] = Object.keys(coneStatuts).map(name => ({ name, statut: coneStatuts[name] } as Cone));

    const endedCuisson = { ...this.cuisson, fin: getTime(fin), observations, statut, cones } as Cuisson;
    this.valider.emit(endedCuisson);
  }
}
