import { Programme } from '@core/models/programme.model';
import { createProgramme } from '@store/reducers/programme.factories';
import {
  addProgramme,
  deleteProgramme, loadProgrammes, loadProgrammesError, loadProgrammesSuccess, updateProgramme
} from './programme.actions';

describe('ProgrammeActions', () => {
  describe('load programme actions', () => {
    it('should create a loading action on loadProgrammes', () => {
      const action = loadProgrammes();
      expect({ ...action }).toEqual({ type: '[Programme] LOAD' });
    });

    it('should create a loading success action on loadProgrammesSuccess', () => {
      const givenLoadedProgrammes: Programme[] = [createProgramme()];
      const action = loadProgrammesSuccess({ programmes: givenLoadedProgrammes });
      expect({ ...action }).toEqual({ type: '[Programme] LOAD Success', programmes: givenLoadedProgrammes });
    });

    it('should create a loading error action on loadProgrammesError', () => {
      const errorMessage = 'An issue occured.';
      const action = loadProgrammesError({ error: errorMessage });
      expect({ ...action }).toEqual({ type: '[Programme] LOAD Error', error: errorMessage });
    });
  });

  describe('edition actions', () => {
    it('should create a creation action on addProgramme', () => {
      const givenProgramme: Programme = createProgramme();
      const action = addProgramme({ programme: givenProgramme });
      expect({ ...action }).toEqual({ type: '[Programme] CREATE', programme: givenProgramme });
    });

    it('should create a update action on updateProgramme', () => {
      const givenProgramme: Programme = createProgramme();
      const action = updateProgramme({ programme: givenProgramme });
      expect({ ...action }).toEqual({ type: '[Programme] UPDATE', programme: givenProgramme });
    });

    it('should create a delete action on deleteProgramme', () => {
      const givenProgramme: Programme = createProgramme();
      const action = deleteProgramme({ programme: givenProgramme });
      expect({ ...action }).toEqual({ type: '[Programme] DELETE', programme: givenProgramme });
    });
  });
});
