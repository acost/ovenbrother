import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Palier } from '@core/models/palier.model';

@Component({
  selector: 'app-palier-input',
  templateUrl: './palier-input.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PalierInputComponent {

  @Input() numero: number;

  @Input() palier: Palier = {} as Palier;

  @Input() enableAdd = false;

  @Input() enableRemove = false;

  @Output() add = new EventEmitter();

  @Output() remove = new EventEmitter<string>();

  onAdd() {
    this.add.emit();
  }

  onRemove() {
    this.remove.emit(this.palier.id);
  }
}
