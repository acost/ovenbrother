import * as fromUser from './user.reducer';
import * as fromActions from './../actions/user.actions';
import { UserActionsUnion } from '../actions/user.actions';

describe('UserReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const { initialState } = fromUser;
      const action = {} as UserActionsUnion;
      const state = fromUser.reducer(undefined, action);

      expect(state).toBe(initialState);
    });
  });

  describe('getUser action', () => {
    it('should set loading to true', () => {
      const { initialState } = fromUser;
      const action = fromActions.getUser();
      const state = fromUser.reducer({ ...initialState, loading: false }, action);

      expect(state.loading).toBe(true);
    });
  });

  describe('googleLogin action', () => {
    it('should set loading to true', () => {
      const { initialState } = fromUser;
      const action = fromActions.googleLogin();
      const state = fromUser.reducer({ ...initialState, loading: false }, action);

      expect(state.loading).toBe(true);
    });
  });

  describe('logout action', () => {
    it('should set loading to true', () => {
      const { initialState } = fromUser;
      const action = fromActions.logout();
      const state = fromUser.reducer({ ...initialState, loading: false }, action);

      expect(state.loading).toBe(true);
    });
  });

  describe('authenticated action', () => {
    it('should set user', () => {
      const givenUid = 'uid';
      const givenDisplayName = 'displayName';

      const { initialState } = fromUser;
      const action = fromActions.authenticated({ uid: givenUid, displayName: givenDisplayName });
      const state = fromUser.reducer({ ...initialState, loading: true }, action);

      expect(state.loading).toBe(false);
      expect(state.user.uid).toBe(givenUid);
      expect(state.user.displayName).toBe(givenDisplayName);
    });
  });

  describe('notAuthenticated action', () => {
    it('should set default user', () => {
      const { initialState, defaultUser } = fromUser;
      const action = fromActions.notAuthenticated();
      const state = fromUser.reducer({ ...initialState, loading: true }, action);

      expect(state.loading).toBe(false);
      expect(state.user).toBe(defaultUser);
    });
  });

  describe('authError action', () => {
    it('should set error', () => {
      const givenErrorMessage = 'An error occured';

      const { initialState } = fromUser;
      const action = fromActions.authError({ error: givenErrorMessage });
      const state = fromUser.reducer({ ...initialState, loading: true }, action);

      expect(state.loading).toBe(false);
      expect(state.error).toBe(givenErrorMessage);
      expect(state.user).toBeUndefined();
    });
  });
});
