import { ActionReducerMap } from '@ngrx/store';
import * as fromProgramme from './reducers/programme.reducer';
import * as fromCuisson from './reducers/cuisson.reducer';
import * as fromUser from './reducers/user.reducer';
import { ProgrammeEffects } from './effects/programme.effects';
import { CuissonEffects } from './effects/cuisson.effects';
import { UserEffects } from './effects/user.effects';
import { NotificationEffects } from './effects/notification.effects';

export interface AppState {
    programmes: fromProgramme.ProgrammeState;
    cuissons: fromCuisson.CuissonState;
    user: fromUser.UserState;
}

export const reducers: ActionReducerMap<AppState> = {
    programmes: fromProgramme.reducer,
    cuissons: fromCuisson.reducer,
    user: fromUser.reducer,
};

export const effects = [
    ProgrammeEffects,
    CuissonEffects,
    UserEffects,
    NotificationEffects,
];
