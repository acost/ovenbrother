import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { Palier } from "@core/models/palier.model";
import { Programme } from "@core/models/programme.model";

@Component({
  selector: "app-programme-form",
  templateUrl: "./programme-form.component.html",
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgrammeFormComponent {
  @Input() programme: Programme = new Programme();

  @Input() enableCancel = true;

  @Output() annuler = new EventEmitter();

  @Output() valider = new EventEmitter<Programme>();

  doSubmit(form: NgForm, event: Event) {
    event.preventDefault();
    const numero: number = form.value.numero;
    const nom: string = form.value.nom;
    const description: string = form.value.description;
    const editionDate: number = Date.now();
    const paliers: Palier[] = form.value.paliers;
    const id = this.programme.id ? this.programme.id : null;

    const newProgramme = new Programme(
      id,
      nom,
      description,
      numero,
      editionDate,
      paliers
    );
    this.valider.emit(newProgramme);
  }

  doCancel(form: NgForm) {
    if (this.programme) {
      form.resetForm({
        numero: this.programme.numero,
        nom: this.programme.nom,
        description: this.programme.description,
        paliers: this.programme.paliers,
      });
    } else {
      form.resetForm();
    }
    this.annuler.emit();
  }
}
