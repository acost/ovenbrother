import { TestBed } from '@angular/core/testing';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { provideMockActions } from '@ngrx/effects/testing';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { UserEffects } from './user.effects';

describe('UserEffects', () => {
  // eslint-disable-next-line prefer-const
  let actions$: Observable<Action>;
  let effects: UserEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserEffects,
        {
          provide: AngularFireAuth,
          useValue: {
            signInWithPopup: jest.fn(),
            signOut: jest.fn(),
          }
        },
        provideMockActions(() => actions$)
      ]
    });
    effects = TestBed.inject(UserEffects);
  });

  it('should be created', async () => {
    expect(effects).toBeTruthy();
  });
});
