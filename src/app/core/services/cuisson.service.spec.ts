import { TestBed } from '@angular/core/testing';

import { CuissonService } from './cuisson.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';

describe('CuissonService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{ provide: AngularFirestore, useValue: {} }]
  }));

  it('should be created', () => {
    const service: CuissonService = TestBed.inject(CuissonService);
    expect(service).toBeTruthy();
  });
});
