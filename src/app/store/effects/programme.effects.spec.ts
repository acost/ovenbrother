import { Observable } from "rxjs";
import { TestBed } from "@angular/core/testing";
import { provideMockActions } from "@ngrx/effects/testing";
import { provideMockStore } from "@ngrx/store/testing";
import { createUser } from "../reducers/user.factories";
import { selectUser } from "../selectors/user.selector";
import { notifySuccess, notifyError } from "../actions/notification.actions";
import { ProgrammeEffects } from "./programme.effects";
import { ProgrammeService } from "../../core/services/programme.service";
import {
  loadProgrammes,
  loadProgrammesSuccess,
  loadProgrammesError,
  addProgramme,
  updateProgramme,
  deleteProgramme,
} from "../actions/programme.actions";
import { createProgramme } from "../reducers/programme.factories";
import { Programme } from "../../core/models/programme.model";
import { TestScheduler } from "rxjs/testing";
import { Action } from "@ngrx/store";

export type Spied<T> = {
  [Method in keyof T]: jest.Mock;
};

describe("ProgrammeEffects", () => {
  let testScheduler: TestScheduler;
  let actions$: Observable<Action>;
  let effects: ProgrammeEffects;
  const programmeServiceMocked = {
    getProgrammes: jest.fn(),
    addProgramme: jest.fn(),
    updateProgramme: jest.fn(),
    removeProgramme: jest.fn(),
  } as any as Spied<ProgrammeService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProgrammeEffects,
        {
          provide: ProgrammeService,
          useValue: programmeServiceMocked,
        },
        provideMockActions(() => actions$),
        provideMockStore({
          selectors: [{ selector: selectUser, value: createUser() }],
        }),
      ],
    });
    effects = TestBed.inject(ProgrammeEffects);
    testScheduler =  new TestScheduler((actual, expected) => {
      expect(actual).toStrictEqual(expected);
    });
  });

  it("should be created", async () => {
    expect(effects).toBeTruthy();
  });

  describe("loadProgrammes", () => {
    it("should return a loadProgrammesSuccess action, with programmes, on success", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenAction = loadProgrammes();
        const expectedLoadedProgrammes: Programme[] = [createProgramme()];
        const expectedOutcome = loadProgrammesSuccess({
          programmes: expectedLoadedProgrammes,
        });

        actions$ = hot("-a", { a: givenAction });
        const response = cold("-a|", { a: expectedLoadedProgrammes });
        programmeServiceMocked.getProgrammes.mockReturnValue(response);

        expectObservable(effects.loadProgrammes$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });

    it("should return a loadCuissonsError action, with error, on failure", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenAction = loadProgrammes();
        const expectedErrorMessage = "An error occured";
        const expectedOutcome = loadProgrammesError({
          error: expectedErrorMessage,
        });

        actions$ = hot("-a", { a: givenAction });
        const response = cold("-#|", {}, new Error(expectedErrorMessage));
        programmeServiceMocked.getProgrammes.mockReturnValue(response);

        expectObservable(effects.loadProgrammes$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });
  });

  describe("addProgramme", () => {
    it("should return a notifySuccess action, with message, on success", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenProgramme: Programme = createProgramme();
        const givenAction = addProgramme({ programme: givenProgramme });
        const expectedOutcome = notifySuccess({
          message: "Programme ajouté avec succès.",
        });

        actions$ = hot("-a", { a: givenAction });
        const response = cold("-a|", { a: {} });
        programmeServiceMocked.addProgramme.mockReturnValue(response);

        expectObservable(effects.addProgrammes$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });

    it("should return a notifyError action, with error, on failure", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenProgramme: Programme = createProgramme();
        const givenAction = addProgramme({ programme: givenProgramme });
        const givenErrorMessage = "An error occured";
        const expectedOutcome = notifyError({ message: givenErrorMessage });

        actions$ = hot("-a", { a: givenAction });
        const response = cold("-#|", {}, new Error(givenErrorMessage));
        programmeServiceMocked.addProgramme.mockReturnValue(response);

        expectObservable(effects.addProgrammes$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });
  });

  describe("updateCuisson", () => {
    it("should return a notifySuccess action, with message, on success", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenProgramme: Programme = createProgramme();
        const givenAction = updateProgramme({ programme: givenProgramme });
        const expectedOutcome = notifySuccess({
          message: "Programme mis à jour avec succès.",
        });

        actions$ = hot("-a", { a: givenAction });
        const response = cold("-a|", { a: {} });
        programmeServiceMocked.updateProgramme.mockReturnValue(response);

        expectObservable(effects.updateProgrammes$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });

    it("should return a notifyError action, with error, on failure", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenProgramme: Programme = createProgramme();
        const givenAction = updateProgramme({ programme: givenProgramme });
        const givenErrorMessage = "An error occured";
        const expectedOutcome = notifyError({ message: givenErrorMessage });

        actions$ = hot("-a", { a: givenAction });
        const response = cold("-#|", {}, new Error(givenErrorMessage));
        const expected = cold("--b", { b: expectedOutcome });
        programmeServiceMocked.updateProgramme.mockReturnValue(response);

        expectObservable(effects.updateProgrammes$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });
  });

  describe("removeCuisson", () => {
    it("should return a notifySuccess action, with message, on success", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenProgramme: Programme = createProgramme();
        const givenAction = deleteProgramme({ programme: givenProgramme });
        const expectedOutcome = notifySuccess({
          message: "Programme supprimé avec succès.",
        });

        actions$ = hot("-a", { a: givenAction });
        const response = cold("-a|", { a: {} });
        programmeServiceMocked.removeProgramme.mockReturnValue(response);

        expectObservable(effects.removeProgrammes$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });

    it("should return a notifyError action, with error, on failure", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenProgramme: Programme = createProgramme();
        const givenAction = deleteProgramme({ programme: givenProgramme });
        const givenErrorMessage = "An error occured";
        const expectedOutcome = notifyError({ message: givenErrorMessage });

        actions$ = hot("-a", { a: givenAction });
        const response = cold("-#|", {}, new Error(givenErrorMessage));
        programmeServiceMocked.removeProgramme.mockReturnValue(response);

        expectObservable(effects.removeProgrammes$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });
  });
});
