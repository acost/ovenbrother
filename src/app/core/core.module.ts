import { NgModule } from "@angular/core";

import { AngularFireModule } from "@angular/fire/compat";
import { AngularFireAuthModule } from "@angular/fire/compat/auth";
import { AngularFirestoreModule } from "@angular/fire/compat/firestore";
import { AngularFireStorageModule } from "@angular/fire/compat/storage";
import { MatButtonModule } from "@angular/material/button";
import { MatDividerModule } from "@angular/material/divider";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatToolbarModule } from "@angular/material/toolbar";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { ServiceWorkerModule } from "@angular/service-worker";
import { AppStoreModule } from "@store/app-store.module";
import { environment } from "@env";
import { ToastrModule } from "ngx-toastr";
import { MainLayoutComponent } from "./layout/main-layout/main-layout.component";
import { NotFoundComponent } from "./not-found/not-found.component";

const config = {
  projectId: "oven-brother",
  apiKey: "AIzaSyCMOFOEOrbPp82k06ygsdjHQGz2Tz2N2gY",
  authDomain: "oven-brother.firebaseapp.com",
  databaseURL: "https://oven-brother.firebaseio.com",
  storageBucket: "oven-brother.appspot.com",
  messagingSenderId: "1019872866859",
  appId: "1:1019872866859:web:aaa20f98bf12825b",
};

@NgModule({
  imports: [
    // vendor
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production,
    }),
    ToastrModule.forRoot({
      newestOnTop: false,
      timeOut: 4000,
      progressBar: true,
    }),
    // material
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatDividerModule,
    MatButtonModule,
    // firebase
    AngularFireModule.initializeApp(config, "ng-oven-brother"),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    AngularFireStorageModule,
    // store
    AppStoreModule,
  ],
  declarations: [NotFoundComponent, MainLayoutComponent],
  exports: [NotFoundComponent, MainLayoutComponent],
})
export class CoreModule {}
