import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ProgrammeState } from '../reducers/programme.reducer';

const selectProgrammesState = createFeatureSelector<ProgrammeState>('programmes');

export const selectProgrammes = createSelector(selectProgrammesState, (state: ProgrammeState) => state.programmes);
export const selectLoading = createSelector(selectProgrammesState, (state: ProgrammeState) => state.loading);
