import * as fromCuisson from './cuisson.reducer';
import * as fromActions from './../actions/cuisson.actions';
import { CuissonActionsUnion } from '../actions/cuisson.actions';
import { Cuisson } from '@core/models/cuisson.model';
import { createCuisson } from './cuisson.factories';

describe('CuissonReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const { initialState } = fromCuisson;
      const action = {} as CuissonActionsUnion;
      const state = fromCuisson.reducer(undefined, action);

      expect(state).toBe(initialState);
    });
  });

  describe('loadCuissons action', () => {
    it('should set loading to true', () => {
      const { initialState } = fromCuisson;
      const action = fromActions.loadCuissons();
      const state = fromCuisson.reducer({ ...initialState, loading: false }, action);

      expect(state.loading).toBe(true);
    });
  });

  describe('loadCuissonsSuccess action', () => {
    it('should set cuissons', () => {
      const givenLoadedCuissons: Cuisson[] = [createCuisson()];
      const { initialState } = fromCuisson;
      const action = fromActions.loadCuissonsSuccess({ cuissons: givenLoadedCuissons });
      const state = fromCuisson.reducer({ ...initialState, loading: true }, action);

      expect(state.loading).toBe(false);
      expect(state.cuissons).toBe(givenLoadedCuissons);
    });
  });

  describe('loadCuissonsError action', () => {
    it('should set error', () => {
      const givenErrorMessage = 'An error occured';
      const { initialState } = fromCuisson;
      const action = fromActions.loadCuissonsError({ error: givenErrorMessage });
      const state = fromCuisson.reducer({ ...initialState, loading: true }, action);

      expect(state.loading).toBe(false);
      expect(state.error).toBe(givenErrorMessage);
    });
  });
});
