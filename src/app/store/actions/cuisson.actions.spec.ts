import { Cuisson } from '@core/models/cuisson.model';
import { createCuisson } from '@store/reducers/cuisson.factories';
import {
  addCuisson,
  deleteCuisson, loadCuissons, loadCuissonsError, loadCuissonsSuccess, updateCuisson
} from './cuisson.actions';

describe('CuissonActions', () => {
  describe('load cuisson actions', () => {
    it('should create a loading action on loadCuissons', () => {
      const action = loadCuissons();
      expect({ ...action }).toEqual({ type: '[Cuisson] LOAD' });
    });

    it('should create a loading success action on loadCuissonsSuccess', () => {
      const givenLoadedCuissons: Cuisson[] = [createCuisson()];
      const action = loadCuissonsSuccess({ cuissons: givenLoadedCuissons });
      expect({ ...action }).toEqual({ type: '[Cuisson] LOAD Success', cuissons: givenLoadedCuissons });
    });

    it('should create a loading error action on loadCuissonsError', () => {
      const errorMessage = 'An issue occured.';
      const action = loadCuissonsError({ error: errorMessage });
      expect({ ...action }).toEqual({ type: '[Cuisson] LOAD Error', error: errorMessage });
    });
  });

  describe('edition actions', () => {
    it('should create a creation action on addCuisson', () => {
      const givenCuisson: Cuisson = createCuisson();
      const action = addCuisson({ cuisson: givenCuisson });
      expect({ ...action }).toEqual({ type: '[Cuisson] CREATE', cuisson: givenCuisson });
    });

    it('should create a update action on updateCuisson', () => {
      const givenCuisson: Cuisson = createCuisson();
      const action = updateCuisson({ cuisson: givenCuisson });
      expect({ ...action }).toEqual({ type: '[Cuisson] UPDATE', cuisson: givenCuisson });
    });

    it('should create a delete action on deleteCuisson', () => {
      const givenCuisson: Cuisson = createCuisson();
      const action = deleteCuisson({ cuisson: givenCuisson });
      expect({ ...action }).toEqual({ type: '[Cuisson] DELETE', cuisson: givenCuisson });
    });
  });
});
