import { notifyError, notifySuccess } from './notification.actions';

describe('NotificationActions', () => {
  it('should create notify success action', () => {
    const givenMessage = 'A success message';
    const action = notifySuccess({ message: givenMessage });
    expect({ ...action }).toEqual({ type: '[Notification] Success', message: givenMessage });
  });

  it('should create notify error action', () => {
    const givenMessage = 'An error message';
    const action = notifyError({ message: givenMessage });
    expect({ ...action }).toEqual({ type: '[Notification] Error', message: givenMessage });
  });
});
