import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { FormsModule } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { PalierInputComponent } from "./palier-input.component";

describe("PalierInputComponent", () => {
  let component: PalierInputComponent;
  let fixture: ComponentFixture<PalierInputComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PalierInputComponent],
      imports: [SharedModule, FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PalierInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
