import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, from, map, mergeMap, of, tap } from 'rxjs';

import { AngularFireAuth } from '@angular/fire/compat/auth';
import * as auth from 'firebase/auth';
import * as UserActions from './../actions/user.actions';


@Injectable()
export class UserEffects {
  getUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.getUser),
      mergeMap(() =>
        this.afAuth.authState.pipe(
          tap(data => console.log(data?.displayName || 'not authenticated')),
          map(authData =>
            authData
              ? UserActions.authenticated({ uid: authData.uid, displayName: authData.displayName || '' })
              : UserActions.notAuthenticated()
          ),
          catchError(err => of(UserActions.authError({ error: err.message })))
        )
      )
    )
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.googleLogin),
      mergeMap(() =>
        from(this.googleLogin()).pipe(
          map(() => UserActions.getUser()),
          catchError(err => of(UserActions.authError({ error: err.message })))
        )
      )
    )
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.logout),
      mergeMap(() =>
        from(this.afAuth.signOut()).pipe(
          map(() => UserActions.notAuthenticated()),
          catchError(err => of(UserActions.authError({ error: err.message })))
        )
      )
    )
  );

  constructor(private actions$: Actions<UserActions.UserActionsUnion>, private afAuth: AngularFireAuth) {}

  private googleLogin(): Promise<any> {
    const provider = new auth.GoogleAuthProvider();
    return this.afAuth.signInWithPopup(provider);
  }
}
