import { getUser, authenticated, notAuthenticated, googleLogin, logout, authError } from './user.actions';

describe('UserActions', () => {
  describe('user state actions', () => {
    it('should create a get user action', () => {
      const action = getUser();
      expect({ ...action }).toEqual({ type: '[User] Get' });
    });

    it('should create an authenticated action', () => {
      const givenUid = 'uid';
      const givenDisplayName = 'displayName';
      const action = authenticated({ uid: givenUid, displayName: givenDisplayName });
      expect({ ...action }).toEqual({ type: '[User] Authenticated', uid: givenUid, displayName: givenDisplayName });
    });

    it('should create a not authenticated action', () => {
      const action = notAuthenticated();
      expect({ ...action }).toEqual({ type: '[User] Not Authenticated' });
    });
  });

  describe('authentification actions', () => {
    it('should create a google login action', () => {
      const action = googleLogin();
      expect({ ...action }).toEqual({ type: '[User] Google login attempt' });
    });

    it('should create a logout action', () => {
      const action = logout();
      expect({ ...action }).toEqual({ type: '[User] Logout' });
    });

    it('should create an authentification error action', () => {
      const givenErrorMessage = 'Auth error';
      const action = authError({ error: givenErrorMessage });
      expect({ ...action }).toEqual({ type: '[User] Authentification Error', error: givenErrorMessage });
    });
  });
});
