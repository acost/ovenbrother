import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { FormsModule } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { PalierGraphComponent } from "@shared/palier-graph/palier-graph.component";
import { ProgrammeModalComponent } from "@shared/programme-modal/programme-modal.component";
import { StatutIconComponent } from "../statut/statut-icon/statut-icon.component";
import { StatutInputComponent } from "../statut/statut-input/statut-input.component";
import { StopCuissonFormComponent } from "./stop-cuisson-form.component";

describe("StopCuissonFormComponent", () => {
  let component: StopCuissonFormComponent;
  let fixture: ComponentFixture<StopCuissonFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        StopCuissonFormComponent,
        ProgrammeModalComponent,
        PalierGraphComponent,
        StatutInputComponent,
        StatutIconComponent,
      ],
      imports: [SharedModule, FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StopCuissonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
