import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PalierGraphComponent } from './palier-graph.component';
import { NgChartsModule } from 'ng2-charts';
import { Palier } from '@core/models/palier.model';

describe('PalierGraphComponent', () => {
  let component: PalierGraphComponent;
  let fixture: ComponentFixture<PalierGraphComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PalierGraphComponent],
      imports: [NgChartsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PalierGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('paliersToLabels', () => {
    it('should return correct value', () => {
      const paliers: Palier[] = [
        { duree: '01:00' } as Palier,
        { duree: '02:30' } as Palier,
        { duree: '05:00' } as Palier,
        { duree: '15:24' } as Palier,
        { duree: '06:06' } as Palier
      ];
      // when
      const formattedDuration = component.paliersToLabels(paliers);
      // then
      expect(formattedDuration).toEqual(['00:00', '01:00', '03:30', '08:30', '23:54', '30:00']);
    });
  });
});
