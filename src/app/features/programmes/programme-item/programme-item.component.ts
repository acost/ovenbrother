import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import { Programme } from "@core/models/programme.model";

@Component({
  selector: "app-programme-item",
  templateUrl: "./programme-item.component.html",
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgrammeItemComponent {
  @Input() programme: Programme;
  @Output() update = new EventEmitter<Programme>();
  @Output() delete = new EventEmitter();

  isEditionMode = false;

  toggleEdition() {
    this.isEditionMode = !this.isEditionMode;
  }

  deleteProgramme() {
    if (window.confirm("Êtes-vous sûr de vouloir supprimer ce programme ?")) {
      this.delete.emit();
    }
  }

  saveProgramme(programme: Programme) {
    this.update.emit(programme);
  }
}
