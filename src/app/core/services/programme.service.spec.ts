import { TestBed } from '@angular/core/testing';

import { ProgrammeService } from './programme.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';

describe('ProgrammeService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [{ provide: AngularFirestore, useValue: {} }]
  }));

  it('should be created', () => {
    const service: ProgrammeService = TestBed.inject(ProgrammeService);
    expect(service).toBeTruthy();
  });
});
