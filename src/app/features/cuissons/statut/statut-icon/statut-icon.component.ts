import { Component, Input } from '@angular/core';
import { StatutCuisson } from '@core/models/cuisson.model';

@Component({
  selector: 'app-statut-icon',
  template: `
    <ng-container [ngSwitch]="statut">
      <i *ngSwitchCase="statutEnum.SOUSCUIT" class="fas fa-snowflake" [style.color]="active ? '#6ea2f4' : ''"></i>
      <i *ngSwitchCase="statutEnum.OK" class="fas fa-check" [style.color]="active ? 'green' : ''"></i>
      <i *ngSwitchCase="statutEnum.SURCUIT" class="fas fa-fire" [style.color]="active ? 'red' : ''"></i>
      <i *ngSwitchDefault class="fas fa-exclamation-triangle" [style.color]="active ? 'orange' : ''"></i>
    </ng-container>
  `,
  styles: []
})
export class StatutIconComponent {

  @Input() statut: StatutCuisson;

  @Input() active = false;

  statutEnum: typeof StatutCuisson = StatutCuisson;
}
