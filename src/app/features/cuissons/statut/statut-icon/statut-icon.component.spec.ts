import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatutIconComponent } from './statut-icon.component';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { StatutCuisson } from '@core/models/cuisson.model';

describe('StatutIconComponent', () => {
  let component: StatutIconComponent;
  let fixture: ComponentFixture<StatutIconComponent>;
  let iconElement: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StatutIconComponent]
    });
    fixture = TestBed.createComponent(StatutIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    iconElement = fixture.debugElement.query(By.css('i'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show an orange warning when no statut given', () => {
    component.active = true;
    fixture.detectChanges();

    expect(iconElement.nativeElement.getAttribute('class')).toEqual('fas fa-exclamation-triangle');
    expect(iconElement.nativeElement.style.color).toEqual('orange');
  });

  it('should show a blue snowflake when sous-cuit', () => {
    component.active = true;
    component.statut = StatutCuisson.SOUSCUIT;
    fixture.detectChanges();

    iconElement = fixture.debugElement.query(By.css('i'));
    expect(iconElement.nativeElement.getAttribute('class')).toEqual('fas fa-snowflake');
    expect(iconElement.nativeElement.style.color).not.toBe('');
  });

  it('should show a green check when ok', () => {
    component.active = true;
    component.statut = StatutCuisson.OK;
    fixture.detectChanges();

    iconElement = fixture.debugElement.query(By.css('i'));
    expect(iconElement.nativeElement.getAttribute('class')).toEqual('fas fa-check');
    expect(iconElement.nativeElement.style.color).toEqual('green');
  });

  it('should show a red fire when sur-cuit', () => {
    component.active = true;
    component.statut = StatutCuisson.SURCUIT;
    fixture.detectChanges();

    iconElement = fixture.debugElement.query(By.css('i'));
    expect(iconElement.nativeElement.getAttribute('class')).toEqual('fas fa-fire');
    expect(iconElement.nativeElement.style.color).toEqual('red');
  });

  it('should change color when triggering active', () => {
    component.active = true;
    fixture.detectChanges();

    expect(iconElement.nativeElement.style.color).not.toEqual('');

    component.active = false;
    fixture.detectChanges();
    expect(iconElement.nativeElement.style.color).toEqual('');
  });
});
