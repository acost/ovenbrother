import { Injectable } from "@angular/core";
import { Actions, ofType, createEffect } from "@ngrx/effects";
import { tap } from "rxjs/operators";
import * as NotificationActions from "../actions/notification.actions";
import { ToastrService } from "ngx-toastr";

@Injectable()
export class NotificationEffects {
  notifySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(NotificationActions.notifySuccess),
        tap((action) => this.toastService.info(action.message))
      ),
    { dispatch: false }
  );

  notifyError$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(NotificationActions.notifyError),
        tap((action) => this.toastService.error(action.message))
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions<NotificationActions.NotificationActionsUnion>,
    private toastService: ToastrService
  ) {}
}
