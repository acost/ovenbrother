import { selectProgrammes, selectLoading } from './programme.selector';
import { ProgrammeState } from '../reducers/programme.reducer';
import { createProgrammeState } from '../reducers/programme.factories';

describe('ProgrammeSelector', () => {
  it('should select programmes', () => {
    const state: ProgrammeState = createProgrammeState();
    expect(selectProgrammes.projector(state)).toBe(state.programmes);
  });

  it('should select loading flag', () => {
    const state: ProgrammeState = createProgrammeState();
    expect(selectLoading.projector(state)).toBe(state.loading);
  });
});
