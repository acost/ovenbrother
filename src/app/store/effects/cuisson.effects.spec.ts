import { CuissonEffects } from "./cuisson.effects";
import { Observable } from "rxjs";
import { TestBed } from "@angular/core/testing";
import { provideMockActions } from "@ngrx/effects/testing";
import {
  loadCuissons,
  loadCuissonsSuccess,
  loadCuissonsError,
  addCuisson,
  updateCuisson,
  deleteCuisson,
} from "../actions/cuisson.actions";
import { Cuisson } from "@core/models/cuisson.model";
import { CuissonService } from "@core/services/cuisson.service";
import { createCuisson } from "../reducers/cuisson.factories";
import { provideMockStore } from "@ngrx/store/testing";
import { createUser } from "../reducers/user.factories";
import { selectUser } from "../selectors/user.selector";
import { notifySuccess, notifyError } from "../actions/notification.actions";
import { TestScheduler } from "rxjs/testing";
import { Action } from "@ngrx/store";

export type Spied<T> = {
  [Method in keyof T]: jest.Mock;
};

describe("CuissonEffects", () => {
  let testScheduler: TestScheduler;
  let actions$: Observable<Action>;
  let effects: CuissonEffects;
  const cuissonServiceMocked = {
    getCuissons: jest.fn(),
    addCuisson: jest.fn(),
    updateCuisson: jest.fn(),
    removeCuisson: jest.fn(),
  } as unknown as Spied<CuissonService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CuissonEffects,
        {
          provide: CuissonService,
          useValue: cuissonServiceMocked,
        },
        provideMockActions(() => actions$),
        provideMockStore({
          selectors: [{ selector: selectUser, value: createUser() }],
        }),
      ],
    });
    effects = TestBed.inject(CuissonEffects);
    testScheduler =  new TestScheduler((actual, expected) => {
      expect(actual).toStrictEqual(expected);
    });
  });

  it("should be created", async () => {
    expect(effects).toBeTruthy();
  });

  describe("loadCuissons", () => {
    it("should return a loadCuissonsSuccess action, with cuissons, on success", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenLoadCuissonAction = loadCuissons();
        const expectedLoadedCuissons: Cuisson[] = [createCuisson()];
        const expectedOutcome = loadCuissonsSuccess({
          cuissons: expectedLoadedCuissons,
        });

        actions$ = hot("-a", { a: givenLoadCuissonAction });
        const response = cold("-a|", { a: expectedLoadedCuissons });
        cuissonServiceMocked.getCuissons.mockReturnValue(response);

        expectObservable(effects.loadCuissons$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });

    it("should return a loadCuissonsError action, with error, on failure", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenLoadCuissonAction = loadCuissons();
        const expectedErrorMessage = "An error occured";
        const expectedOutcome = loadCuissonsError({
          error: expectedErrorMessage,
        });

        actions$ = hot("-a", { a: givenLoadCuissonAction });
        const response = cold("-#|", {}, new Error(expectedErrorMessage));
        cuissonServiceMocked.getCuissons.mockReturnValue(response);

        expectObservable(effects.loadCuissons$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });
  });

  describe("addCuisson", () => {
    it("should return a notifySuccess action, with message, on success", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenCuisson: Cuisson = createCuisson();
        const givenAddCuissonAction = addCuisson({ cuisson: givenCuisson });
        const expectedOutcome = notifySuccess({
          message: "Programme ajouté avec succès.",
        });

        actions$ = hot("-a", { a: givenAddCuissonAction });
        const response = cold("-a|", { a: {} });
        cuissonServiceMocked.addCuisson.mockReturnValue(response);

        expectObservable(effects.addCuisson$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });

    it("should return a notifyError action, with error, on failure", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenCuisson: Cuisson = createCuisson();
        const givenAddCuissonAction = addCuisson({ cuisson: givenCuisson });
        const givenErrorMessage = "An error occured";
        const expectedOutcome = notifyError({ message: givenErrorMessage });

        actions$ = hot("-a", { a: givenAddCuissonAction });
        const response = cold("-#|", {}, new Error(givenErrorMessage));
        cuissonServiceMocked.addCuisson.mockReturnValue(response);

        expectObservable(effects.addCuisson$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });
  });

  describe("updateCuisson", () => {
    it("should return a notifySuccess action, with message, on success", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenCuisson: Cuisson = createCuisson();
        const givenUpdateCuissonAction = updateCuisson({
          cuisson: givenCuisson,
        });
        const expectedOutcome = notifySuccess({
          message: "Programme mis à jour avec succès.",
        });

        actions$ = hot("-a", { a: givenUpdateCuissonAction });
        const response = cold("-a|", { a: {} });
        cuissonServiceMocked.updateCuisson.mockReturnValue(response);

        expectObservable(effects.updateCuisson$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });

    it("should return a notifyError action, with error, on failure", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenCuisson: Cuisson = createCuisson();
        const givenUpdateCuissonAction = updateCuisson({
          cuisson: givenCuisson,
        });
        const givenErrorMessage = "An error occured";
        const expectedOutcome = notifyError({ message: givenErrorMessage });

        actions$ = hot("-a", { a: givenUpdateCuissonAction });
        const response = cold("-#|", {}, new Error(givenErrorMessage));
        cuissonServiceMocked.updateCuisson.mockReturnValue(response);

        expectObservable(effects.updateCuisson$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });
  });

  describe("removeCuisson", () => {
    it("should return a notifySuccess action, with message, on success", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenCuisson: Cuisson = createCuisson();
        const givenDeleteCuissonAction = deleteCuisson({
          cuisson: givenCuisson,
        });
        const expectedOutcome = notifySuccess({
          message: "Programme supprimé avec succès.",
        });

        actions$ = hot("-a", { a: givenDeleteCuissonAction });
        const response = cold("-a|", { a: {} });
        cuissonServiceMocked.removeCuisson.mockReturnValue(response);

        expectObservable(effects.removeCuisson$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });

    it("should return a notifyError action, with error, on failure", () => {
      testScheduler.run(({ hot, cold, expectObservable }) => {
        const givenCuisson: Cuisson = createCuisson();
        const givenDeleteCuissonAction = deleteCuisson({
          cuisson: givenCuisson,
        });
        const givenErrorMessage = "An error occured";
        const expectedOutcome = notifyError({ message: givenErrorMessage });

        actions$ = hot("-a", { a: givenDeleteCuissonAction });
        const response = cold("-#|", {}, new Error(givenErrorMessage));
        cuissonServiceMocked.removeCuisson.mockReturnValue(response);

        expectObservable(effects.removeCuisson$).toBe("--b", {
          b: expectedOutcome,
        });
      });
    });
  });
});
