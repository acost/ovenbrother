import {
  Component, OnInit, ChangeDetectionStrategy, ViewChild, Input,
  EventEmitter, Output, OnChanges, SimpleChanges
} from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Cuisson } from '@core/models/cuisson.model';
import { Programme } from '@core/models/programme.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { differenceInHours, differenceInMinutes } from 'date-fns';

@Component({
  selector: 'app-cuisson-table',
  templateUrl: './cuisson-table.component.html',
  styleUrls: ['./cuisson-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CuissonTableComponent implements OnInit, OnChanges {
  @Input() cuissons: Cuisson[];
  @Input() programmes: Programme[];

  @Output() delete = new EventEmitter<Cuisson>();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = ['date', 'programme', 'duration', 'cones', 'statut'];
  dataSource: MatTableDataSource<Cuisson>;
  expandedElement: Cuisson | null;

  constructor() {
    this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case 'date': return item.debut;
        case 'duration': return this.calculateDureeCuisson(item);
        default: return item[property];
      }
    };

    if (this.sort) {
      this.sort.sort({ id: 'date', start: 'desc', disableClear: false });
      // reset the paginator after sorting
      this.sort.sortChange.subscribe(() => this.paginator.firstPage());
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.cuissons && changes.cuissons.currentValue) {
      this.dataSource.data = changes.cuissons.currentValue;
      this.paginator.firstPage();
    }
  }

  isCuissonActive = cuisson => !cuisson.fin;

  calculateDureeCuisson(cuisson: Cuisson): string {
    const dateFin = this.isCuissonActive(cuisson) ? Date.now() : cuisson.fin;
    const hours = differenceInHours(dateFin, cuisson.debut).toString().padStart(2, '0');
    const minutes = (differenceInMinutes(dateFin, cuisson.debut) % 60).toString().padStart(2, '0');
    return `${hours}h${minutes}`;
  }

  getProgrammeMatching(cuisson: Cuisson): Programme {
    return cuisson && cuisson.programme
      ? this.programmes.find(programme => programme.id === cuisson.programme)
      : undefined;
  }

  triggerDelete(cuisson: Cuisson) {
    if (window.confirm('Êtes-vous sûr de vouloir supprimer cette cuisson ?')) {
      this.delete.emit(cuisson);
    }
  }
}
