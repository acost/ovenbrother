import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StatutInputComponent } from './statut-input.component';
import { StatutIconComponent } from '../statut-icon/statut-icon.component';

describe('StatutInputComponent', () => {
  let component: StatutInputComponent;
  let fixture: ComponentFixture<StatutInputComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StatutInputComponent, StatutIconComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatutInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
