export enum StatutCuisson {
    SOUSCUIT = 'SOUSCUIT',
    OK = 'OK',
    SURCUIT = 'SURCUIT'
}

export class Cuisson {
    id: string;
    debut: number;
    programme: string;
    description: string;
    finCuisson: number;
    cones: Cone[];

    fin: number;
    observations: string;
    statut: StatutCuisson;

    images?: string[];

    constructor(id: string, programme: string, debut: number, description: string, cones: Cone[], finCuisson: number) {
        this.id = id;
        this.programme = programme;
        this.debut = debut;
        this.description = description;
        this.cones = cones;
        this.finCuisson = finCuisson;
    }
}

export class Cone {
    name: string;
    statut: StatutCuisson;
}
