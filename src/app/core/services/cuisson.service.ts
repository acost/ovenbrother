import { Injectable } from '@angular/core';
import { Cuisson } from '../models/cuisson.model';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class CuissonService {

  constructor(private db: AngularFirestore) { }

  getCuissons(uid: string): Observable<Cuisson[]> {
    if (!uid) {
      return from([]);
    }

    const collection = this.db.collection<Cuisson>(`users/${uid}/cuissons`);
    return collection.snapshotChanges()
      .pipe(map(values =>
        values.map(c => ({ ...c.payload.doc.data(), id: c.payload.doc.id } as Cuisson)))) as Observable<any> as Observable<Cuisson[]>;
  }

  addCuisson(uid: string, cuisson: Cuisson) {
    const collection = this.db.collection<Cuisson>(`users/${uid}/cuissons`);
    return from(collection.add(JSON.parse(JSON.stringify(cuisson))));
  }

  updateCuisson(uid: string, itemId: string, cuisson: Cuisson) {
    const collection = this.db.collection<Cuisson>(`users/${uid}/cuissons`);
    return from(collection.doc(itemId).update(JSON.parse(JSON.stringify(cuisson))));
  }

  removeCuisson(uid: string, itemId: string) {
    const collection = this.db.collection<Cuisson>(`users/${uid}/cuissons`);
    return from(collection.doc(itemId).delete());
  }
}
