import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-found',
  template: `
    <div class="container">
      <h2>Aucune page trouvée à l'adresse <code>{{href}}</code></h2>
    </div>
  `,
  styles: []
})
export class NotFoundComponent implements OnInit {

  href = '';

  constructor(private router: Router) { }

  ngOnInit() {
    this.href = this.router.url;
  }

}
