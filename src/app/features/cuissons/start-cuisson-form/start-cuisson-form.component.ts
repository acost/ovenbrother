import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { addMilliseconds, differenceInMilliseconds, format, getTime, parse } from 'date-fns';
import { Cone, Cuisson } from '@core/models/cuisson.model';
import { Programme } from '@core/models/programme.model';

@Component({
  selector: 'app-start-cuisson-form',
  templateUrl: './start-cuisson-form.component.html',
  styles: []
})
export class StartCuissonFormComponent {

  @Input() programmes: Programme[];
  @Output() valider = new EventEmitter<Cuisson>();

  @Input() dateDebut: string = format(new Date(), 'yyyy-MM-dd');
  @Input() heureDebut: string = format(new Date(), 'HH:mm');
  dateFin: string;
  selectedProgramme: Programme;

  doSubmit(form: NgForm, event: Event) {
    event.preventDefault();

    const { dateDebut, heureDebut, programme, description, ...cones } = form.value;
    const debut: Date = parse(`${dateDebut}, ${heureDebut}`, 'yyyy-MM-dd, HH:mm', new Date());
    const finCuisson = this.calculateEndDate(debut, this.selectedProgramme);
    const checkedCones: Cone[] = Object.keys(cones)
      .filter(cone => cones[cone] === true)
      .map(name => ({ name } as Cone));
    const newCuisson: Cuisson = new Cuisson(null, programme.id, debut.valueOf(), description, checkedCones, getTime(finCuisson));
    this.valider.emit(newCuisson);
  }

  onDateOrProgrammeChange() {
    const debut: Date = parse(`${this.dateDebut}, ${this.heureDebut}`, 'yyyy-MM-dd, HH:mm', new Date());
    this.dateFin = this.calculateEndDateFormatted(debut, this.selectedProgramme);
  }

  private calculateEndDateFormatted(debut: Date, selectedProgramme: Programme): string {
    if (!debut || !selectedProgramme) {
      return '';
    }
    return new Date(this.calculateEndDate(debut, selectedProgramme)).toLocaleString();
  }

  private calculateEndDate(debut: Date, selectedProgramme: Programme): Date {
    const millisecondDuration: number = this.calculateDurationInMilliseconds(selectedProgramme);
    return addMilliseconds(debut, millisecondDuration);
  }

  private calculateDurationInMilliseconds(programme: Programme): number {
    if (!programme) {
      return null;
    }
    const { paliers } = programme;
    let millisecondDuration = 0;
    Object.keys(paliers)
      .forEach(index => {
        if (paliers[index].duree) {
          millisecondDuration += this.getMillisecondsForDurationString(paliers[index].duree);
        }
      });
    return millisecondDuration;
  }

  private getMillisecondsForDurationString(durationString: string): number {
    const baseHour = parse('00:00', 'HH:mm', 0);
    return differenceInMilliseconds(parse(durationString, 'HH:mm', 0), baseHour);
  }
}
