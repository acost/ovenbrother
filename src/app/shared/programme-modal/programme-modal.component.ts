import { Component, Input } from "@angular/core";
import { Programme } from "@core/models/programme.model";

@Component({
  selector: "app-programme-modal",
  templateUrl: "./programme-modal.component.html",
  styles: [],
})
export class ProgrammeModalComponent {
  @Input() programme: Programme;
}
