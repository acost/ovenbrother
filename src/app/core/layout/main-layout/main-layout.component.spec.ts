import { ComponentFixture, TestBed } from "@angular/core/testing";
import { SwUpdate } from "@angular/service-worker";
import { provideMockStore } from "@ngrx/store/testing";
import { UserState } from "@store/reducers/user.reducer";

import { MainLayoutComponent } from "./main-layout.component";

describe("MainLayoutComponent", () => {
  const initialState: UserState = { loading: false };

  let component: MainLayoutComponent;
  let fixture: ComponentFixture<MainLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MainLayoutComponent],
      providers: [
        provideMockStore({ initialState }),
        { provide: SwUpdate, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MainLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
