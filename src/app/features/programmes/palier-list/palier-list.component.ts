import { ChangeDetectionStrategy, Component, Input } from "@angular/core";
import { Palier } from "@core/models/palier.model";

@Component({
  selector: "app-palier-list",
  templateUrl: "./palier-list.component.html",
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PalierListComponent {
  @Input() paliers: Palier[];
}
