import { Component } from "@angular/core";
import { Store } from "@ngrx/store";
import { Programme } from "@core/models/programme.model";
import { addProgramme } from "@store/actions/programme.actions";
import { ProgrammeState } from "@store/reducers/programme.reducer";

@Component({
  selector: "app-programmes",
  templateUrl: "./programmes.component.html",
  styles: [],
})
export class ProgrammesComponent {
  constructor(private store: Store<ProgrammeState>) {}

  addProgramme(programme: Programme) {
    this.store.dispatch(addProgramme({ programme }));
  }
}
