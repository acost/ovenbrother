import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Programme } from '@core/models/programme.model';
import { select, Store } from '@ngrx/store';
import * as ProgrammeActions from '@store/actions/programme.actions';
import { ProgrammeState } from '@store/reducers/programme.reducer';
import * as fromProgrammes from '@store/selectors/programme.selector';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-programme-list',
  templateUrl: './programme-list.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgrammeListComponent implements OnInit {
  programmes$: Observable<Programme[]>;
  isLoading$: Observable<boolean>;

  constructor(private store: Store<ProgrammeState>) {
    this.programmes$ = store.pipe(select(fromProgrammes.selectProgrammes));
    this.isLoading$ = store.pipe(select(fromProgrammes.selectLoading));
  }

  ngOnInit() {
    this.store.dispatch(ProgrammeActions.loadProgrammes());
  }

  doUpdate(programme: Programme) {
    this.store.dispatch(ProgrammeActions.updateProgramme({ programme }));
  }

  doDelete(programme: Programme) {
    this.store.dispatch(ProgrammeActions.deleteProgramme({ programme }));
  }
}
