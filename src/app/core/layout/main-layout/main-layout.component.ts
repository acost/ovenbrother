import { MediaMatcher } from "@angular/cdk/layout";
import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SwUpdate } from "@angular/service-worker";
import {
  getUser,
  googleLogin,
  logout,
} from "@store/actions/user.actions";
import { UserState } from "@store/reducers/user.reducer";
import { selectLoading } from "@store/selectors/programme.selector";
import { selectUser } from "@store/selectors/user.selector";
import { Store, select } from "@ngrx/store";
import { User } from "../../models/user.model";
import { Observable, tap } from "rxjs";
import packageJson from "../../../../../package.json";

@Component({
  selector: "app-main-layout",
  templateUrl: "./main-layout.component.html",
  styleUrls: ["./main-layout.component.scss"],
})
export class MainLayoutComponent implements OnInit {
  appVersion: string = packageJson.version;

  user$: Observable<User>;
  isLoading$: Observable<boolean>;

  mobileQuery: MediaQueryList;
  private mobileQueryListener: () => void;

  private HOME_REGEX = /^\/(?:\?.*)?$/;
  isHomePage: boolean;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private store: Store<UserState>,
    private swUpdate: SwUpdate
  ) {
    this.mobileQuery = media.matchMedia("(max-width: 600px)");
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this.mobileQueryListener);

    router.events
      .pipe(
        tap(() => {
          const isHome = router.url && router.url.match(this.HOME_REGEX);
          this.isHomePage = (isHome && isHome.length > 0) || false;
        })
      )
      .subscribe();

    this.user$ = store.pipe(select(selectUser));
    this.isLoading$ = store.pipe(select(selectLoading));
  }

  ngOnInit() {
    this.store.dispatch(getUser());
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm("New version available. Load New Version?")) {
          window.location.reload();
        }
      });
      this.swUpdate.checkForUpdate();
    }
  }

  doGoogleLogin() {
    this.store.dispatch(googleLogin());
    this.router.navigate(["/"]);
  }

  doLogout() {
    this.store.dispatch(logout());
    this.router.navigate(["/"]);
  }

  isLogged(user: User) {
    return user.uid !== null;
  }
}
