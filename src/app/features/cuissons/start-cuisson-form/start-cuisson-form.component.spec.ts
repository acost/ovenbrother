import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";

import { FormsModule } from "@angular/forms";
import { SharedModule } from "@shared/shared.module";
import { PalierGraphComponent } from "@shared/palier-graph/palier-graph.component";
import { ProgrammeModalComponent } from "@shared/programme-modal/programme-modal.component";
import { StartCuissonFormComponent } from "./start-cuisson-form.component";

describe("StartCuissonFormComponent", () => {
  let component: StartCuissonFormComponent;
  let fixture: ComponentFixture<StartCuissonFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        StartCuissonFormComponent,
        ProgrammeModalComponent,
        PalierGraphComponent,
      ],
      imports: [SharedModule, FormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartCuissonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
