import { createCuissonState, createCuisson } from '../reducers/cuisson.factories';
import { CuissonState } from '../reducers/cuisson.reducer';
import { selectCuissons, selectLoading, selectActiveCuisson, selectEndedCuissons } from './cuisson.selector';
import { Cuisson } from '@core/models/cuisson.model';

describe('CuissonSelector', () => {
  it('should select cuissons', () => {
    const state: CuissonState = createCuissonState();
    expect(selectCuissons.projector(state)).toBe(state.cuissons);
  });

  it('should select loading flag', () => {
    const state: CuissonState = createCuissonState();
    expect(selectLoading.projector(state)).toBe(state.loading);
  });

  describe('select filtered cuissons', () => {
    const givenActiveCuisson: Cuisson = createCuisson({ id: 'ACTIVE_1', fin: undefined });
    const givenEndedCuissons: Cuisson[] = [
      createCuisson({ id: 'ENDED_1', fin: 12345789 }),
      createCuisson({ id: 'ENDED_2', fin: 12345789 })
    ];

    it('should select active cuissons', () => {
      expect(selectActiveCuisson.projector([givenActiveCuisson, ...givenEndedCuissons])).toBe(givenActiveCuisson);
    });

    it('should select ended cuissons', () => {
      expect(selectEndedCuissons.projector([givenActiveCuisson, ...givenEndedCuissons])).toEqual(givenEndedCuissons);
    });
  });
});
