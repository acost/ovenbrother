import * as fromProgramme from './programme.reducer';
import * as fromActions from './../actions/programme.actions';
import { ProgrammeActionsUnion } from '../actions/programme.actions';
import { Programme } from '@core/models/programme.model';
import { createProgramme } from './programme.factories';

describe('ProgrammeReducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const { initialState } = fromProgramme;
      const action = {} as ProgrammeActionsUnion;
      const state = fromProgramme.reducer(undefined, action);

      expect(state).toBe(initialState);
    });
  });

  describe('loadProgrammes action', () => {
    it('should set loading to true', () => {
      const { initialState } = fromProgramme;
      const action = fromActions.loadProgrammes();
      const state = fromProgramme.reducer({ ...initialState, loading: false }, action);

      expect(state.loading).toBe(true);
    });
  });

  describe('loadProgrammesSuccess action', () => {
    it('should set cuissons', () => {
      const givenLoadedProgrammes: Programme[] = [createProgramme()];
      const { initialState } = fromProgramme;
      const action = fromActions.loadProgrammesSuccess({ programmes: givenLoadedProgrammes });
      const state = fromProgramme.reducer({ ...initialState, loading: true }, action);

      expect(state.loading).toBe(false);
      expect(state.programmes).toBe(givenLoadedProgrammes);
    });
  });

  describe('loadProgrammesError action', () => {
    it('should set error', () => {
      const givenErrorMessage = 'An error occured';
      const { initialState } = fromProgramme;
      const action = fromActions.loadProgrammesError({ error: givenErrorMessage });
      const state = fromProgramme.reducer({ ...initialState, loading: true }, action);

      expect(state.loading).toBe(false);
      expect(state.error).toBe(givenErrorMessage);
    });
  });
});
