import { createFeatureSelector, createSelector } from '@ngrx/store';
import { CuissonState } from '../reducers/cuisson.reducer';
import { Cuisson } from '@core/models/cuisson.model';

const selectCuissonsState = createFeatureSelector<CuissonState>('cuissons');

export const selectLoading = createSelector(selectCuissonsState, (state: CuissonState) => state.loading);

export const selectCuissons = createSelector(selectCuissonsState, (state: CuissonState) => state.cuissons);
export const selectActiveCuisson = createSelector(selectCuissons, (cuissons: Cuisson[]) => cuissons.find(cuisson => !cuisson.fin));
export const selectEndedCuissons = createSelector(selectCuissons, (cuissons: Cuisson[]) => cuissons.filter(cuisson => cuisson.fin));
