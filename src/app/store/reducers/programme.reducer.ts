import { createReducer, on } from '@ngrx/store';
import * as ProgrammeActions from './../actions/programme.actions';
import { Programme } from '@core/models/programme.model';

export interface ProgrammeState {
    programmes: Programme[];
    loading: boolean;
    error?: string;
}

export const initialState: ProgrammeState = {
    loading: false,
    programmes: []
};

const programmeReducer = createReducer(
    initialState,
    on(ProgrammeActions.loadProgrammes, state => ({
        ...state,
        loading: true
    })),
    on(ProgrammeActions.loadProgrammesSuccess, (state, { programmes }) => ({
        ...state,
        loading: false,
        programmes
    })),
    on(ProgrammeActions.loadProgrammesError, (state, { error }) => ({
        ...state,
        loading: false,
        error
    })),
);

export function reducer(state: ProgrammeState = initialState, action: ProgrammeActions.ProgrammeActionsUnion) {
    return programmeReducer(state, action);
}
