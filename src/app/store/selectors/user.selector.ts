import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserState } from '../reducers/user.reducer';

const selectUserState = createFeatureSelector<UserState>('user');

export const selectLoading = createSelector(selectUserState, (state: UserState) => state.loading);
export const selectUser = createSelector(selectUserState, (state: UserState) => state.user);
