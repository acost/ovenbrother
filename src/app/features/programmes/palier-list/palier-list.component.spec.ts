import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PalierListComponent } from './palier-list.component';
import { PalierGraphComponent } from '@shared/palier-graph/palier-graph.component';
import { NgChartsModule } from 'ng2-charts';

describe('PalierListComponent', () => {
  let component: PalierListComponent;
  let fixture: ComponentFixture<PalierListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [PalierListComponent, PalierGraphComponent],
      imports: [NgChartsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PalierListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
