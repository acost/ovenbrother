import { createAction, props, union } from '@ngrx/store';

export const getUser = createAction('[User] Get');
export const authenticated = createAction('[User] Authenticated', props<{ uid: string, displayName: string }>());
export const notAuthenticated = createAction('[User] Not Authenticated');

export const googleLogin = createAction('[User] Google login attempt');
export const logout = createAction('[User] Logout');

export const authError = createAction('[User] Authentification Error', props<{ error?: string }>());

const actions = union({ getUser, authenticated, notAuthenticated, googleLogin, logout, authError });
export type UserActionsUnion = typeof actions;
