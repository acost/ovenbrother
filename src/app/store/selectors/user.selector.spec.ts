import { selectUser, selectLoading } from './user.selector';
import { createUserState } from '../reducers/user.factories';
import { UserState } from '../reducers/user.reducer';

describe('UserSelector', () => {
  it('should select current user', () => {
    const state: UserState = createUserState();
    expect(selectUser.projector(state)).toBe(state.user);
  });

  it('should select loading flag', () => {
    const state: UserState = createUserState();
    expect(selectLoading.projector(state)).toBe(state.loading);
  });
});
