import { Component, OnInit } from "@angular/core";
import { Programme } from "@core/models/programme.model";
import { select, Store } from "@ngrx/store";
import {
  addCuisson,
  deleteCuisson,
  loadCuissons,
  updateCuisson
} from "@store/actions/cuisson.actions";
import { loadProgrammes } from "@store/actions/programme.actions";
import { AppState } from "@store/app.reducer";
import * as CuissonSelectors from "@store/selectors/cuisson.selector";
import * as ProgrammeSelectors from "@store/selectors/programme.selector";
import { Observable } from "rxjs";
import { Cuisson } from "@core/models/cuisson.model";

@Component({
  selector: "app-cuissons",
  templateUrl: "./cuissons.component.html",
  styles: [],
})
export class CuissonsComponent implements OnInit {
  programmes$: Observable<Programme[]>;
  programmesLoading$: Observable<boolean>;

  activeCuisson$: Observable<Cuisson>;
  cuissons$: Observable<Cuisson[]>;
  cuissonsLoading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {
    this.programmes$ = store.pipe(select(ProgrammeSelectors.selectProgrammes));
    this.programmesLoading$ = store.pipe(
      select(ProgrammeSelectors.selectLoading)
    );

    this.activeCuisson$ = store.pipe(
      select(CuissonSelectors.selectActiveCuisson)
    );
    this.cuissons$ = store.pipe(select(CuissonSelectors.selectEndedCuissons));
    this.cuissonsLoading$ = store.pipe(select(CuissonSelectors.selectLoading));
  }

  ngOnInit() {
    this.store.dispatch(loadProgrammes());
    this.store.dispatch(loadCuissons());
  }

  addCuisson(cuisson: Cuisson) {
    this.store.dispatch(addCuisson({ cuisson }));
  }

  terminerCuisson(cuisson: Cuisson) {
    this.store.dispatch(updateCuisson({ cuisson }));
  }

  supprimerCuisson(cuisson: Cuisson) {
    this.store.dispatch(deleteCuisson({ cuisson }));
  }

  getProgrammeMatching(cuisson: Cuisson, programmes: Programme[]): Programme {
    return cuisson && cuisson.programme
      ? programmes.find((programme) => programme.id === cuisson.programme)
      : undefined;
  }
}
