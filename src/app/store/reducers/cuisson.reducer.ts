import { createReducer, on } from '@ngrx/store';
import * as CuissonActions from '../actions/cuisson.actions';
import { Cuisson } from '@core/models/cuisson.model';

export interface CuissonState {
  cuissons: Cuisson[];
  loading: boolean;
  error?: string;
}

export const initialState: CuissonState = {
  loading: false,
  cuissons: []
};

const programmeReducer = createReducer(
  initialState,
  on(CuissonActions.loadCuissons, state => ({
    ...state,
    loading: true
  })),
  on(CuissonActions.loadCuissonsSuccess, (state, { cuissons }) => ({
    ...state,
    loading: false,
    cuissons
  })),
  on(CuissonActions.loadCuissonsError, (state, { error }) => ({
    ...state,
    loading: false,
    error
  }))
);

export function reducer(state: CuissonState = initialState, action: CuissonActions.CuissonActionsUnion) {
  return programmeReducer(state, action);
}
