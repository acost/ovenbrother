import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { reducers, effects } from './app.reducer';

@NgModule({
    imports: [
        StoreModule.forRoot(reducers, {
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
                // strictStateSerializability: true,
                // strictActionSerializability: true,
            },
        }),
        EffectsModule.forRoot(effects),
        StoreDevtoolsModule.instrument({ maxAge: 50 })
    ],
    exports: [StoreModule]
})
export class AppStoreModule { }
